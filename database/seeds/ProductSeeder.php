<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //buat data ke tabel products untuk testing
        Product::create([
            'category_id' => '1',
            'name' => 'kentang',
            'price' => '10000',
            'discount' => '2000',
            'stock' => '20',
        ]);
    }
}
