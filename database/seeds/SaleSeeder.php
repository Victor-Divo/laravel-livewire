<?php

use Illuminate\Database\Seeder;
use App\Sale;

class SaleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //buat data ke tabel sales untuk testing
        Sale::create([
            'user_id' => '1',
            'datetime' => date('Y-m-d H:i:s'),
            'grandtotal' => '8000',
            'payment' => 'cash'
        ]);
    }
}
