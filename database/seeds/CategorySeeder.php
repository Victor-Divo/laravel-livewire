<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //tambah data ke tabel categories untuk testing
        Category::create([
            'name' => 'makanan-ringan',
        ]);
    }
}
