<?php

use Illuminate\Database\Seeder;
use App\SaleDetail;

class SaleDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        SaleDetail::create([
            'sale_id' => '1',
            'product_id' => '1',
            'price' => '10000',
            'discount' => '2000',
            'qty' => '2',
            'subtotal' => '8000',
            
        ]);
    }
}
