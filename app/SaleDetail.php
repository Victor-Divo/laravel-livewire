<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaleDetail extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'sale_id', 'product_id', 'price', 'discount', 'qty', 'subtotal',
    ];

    public function sale ()
    {
        return $this->belongsTo(Sale::class);
    }

    public function products ()
    {
        return $this->belongsToMany(Product::class);
    }

}
