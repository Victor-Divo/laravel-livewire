<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Sale extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'datetime', 'grandtotal', 'payment',
    ];

    public function saleDetail()
    {
        return $this->hasOne(SaleDetail::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}

