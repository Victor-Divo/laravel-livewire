<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use App\User;

class UserUpdate extends Component
{
    public $name;
    public $email;
    public $userId;
    
    protected $listeners = [
        'getUser' => 'showUser'
    ];

    public function render()
    {
        return view('livewire.user-update');
    }

    public function showUser($user)
    {
        $this->name = $user['name'];
        $this->email = $user['email'];
        $this->userId = $user['id'];
        // dd($user);

    }

    public function update()
    {
        $this->validate([
            'name' => 'required|min:3',
            'email' => 'required',


        ]);

        if ( $this->userId ) {
            $user = User::find($this->userId);
            $user->update([
            'name' => $this->name,
            'email' => $this->email,
            'password' => Hash::make(12345678),
            'role' => 'user',
            ]);

            $this->resetInput();

            $this->emit('userUpdated', $user);
        }
    }

    private function resetInput()
    {
        $this->name = null;
        $this->email = null;
    }
}
