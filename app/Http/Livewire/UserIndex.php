<?php

namespace App\Http\Livewire;

use Livewire\WithPagination;
use Livewire\Component;
use App\User;

class UserIndex extends Component
{
   
    use WithPagination;
    public $statusUpdate = false;
    public $conf;
    public $paginate = 3;
    public $search;

    protected $listeners = [
        'userStored' => 'handleStored',
        'userUpdated' => 'handleUpdated'
    ];
    public function render()
    {
        
        return view('livewire.user-index', [
            'users' => $this->search === null ?
            User::latest()->paginate($this->paginate) :
            User::latest()->where('name', 'like', '%' . $this->search . '%')->paginate($this->paginate)

        ]);
    }

    public function getUser($id)
    {
        $this->statusUpdate = true;
        $user = User::find($id);
        $this->emit('getUser', $user);
    }

    public function destroy($id, $conf)
    {
        
        if($id && $conf){
            $data = User::find($id);
            $data->delete();
            session()->flash('pesan', 'User berhasil dihapus!');
            

        }
    }

    public function handleStored($user)
    {
        session()->flash('pesan', 'User ' . $user['name'] . ' telah tersimpan!');
    }

    public function handleUpdated($user)
    {
        session()->flash('pesan', 'User ' . $user['name'] . ' telah diperbarui!');
    }
}
