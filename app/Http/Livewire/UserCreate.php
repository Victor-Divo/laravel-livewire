<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use App\User;


class UserCreate extends Component
{
    public $name;
    public $email;

    public function render()
    {
        return view('livewire.user-create');
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'name' => 'required|min:3|max:255',
            'email' => 'required|email|unique:users|max:255',
        ]);
    }


    public function store()
    {

        
        $this->validate([
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',

        ]);



        $user = User::create([
            'name' => $this->name,
            'email' => $this->email,
            'password' =>Hash::make(12345678),
            'role' => 'user',
        ]);
        
        

        $this->resetInput();

        $this->emit('userStored', $user);
    }

    // private function encryptPassword($password)
    // {
    //     $password = Crypt::encrypt($password);
    //     return($password);
       
    // }

    private function resetInput()
    {
        $this->name = null;
        $this->email = null;
    }

}
