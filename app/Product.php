<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Product extends Model
{
    use SoftDeletes;

    protected $fillable =[
        'category_id', 'name', 'price', 'discount', 'stock' 
    ];

    public function saleDetails()
    {
        return $this->belongsToMany(SaleDetail::class);
    }

    public function categories()
    {
        return belongsToMany(Category::class);
    }

}