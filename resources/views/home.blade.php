@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <p>
                Hi <strong>{{ auth()->user()->name }}</strong>
                Anda login sebagai
                @can('isAdmin')
                <span class="btn btn-success btn-sm">Admin</span>
                @else
                <span class="btn btn-primary btn-sm">User</span>
                @endcan
            </p>
            <livewire:user-index>  </livewire:user-index>

        </div>
    </div>
</div>
@endsection
