<div>
    @if (session()->has('pesan'))
        <div class="alert alert-success">
            {{ session('pesan') }}
        </div>
    @endif


    @can('isAdmin')
        @if ($statusUpdate)
            <livewire:user-update></livewire:user-update>
        @else
            <livewire:user-create></livewire:user-create>
        @endif
    @endcan


    <hr>

    <div class="row">
        <div class="col">
            <select wire:model="paginate" name="" id="" class="form-control form-control-sm w-auto">
                <option value="3">3</option>
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
            </select>
        </div>
        <div class="col">
            <input wire:model="search" type="text" class="form-control form-control-sm" placeholder="search" >
        </div>
    </div>

    <hr>

    <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Email</th>
            <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ( $users as $user )
                <tr>
                    <th scope="row">{{ $loop -> iteration}}</th>
                    <td>{{ $user -> name }}</td>
                    <td>{{ $user -> email }}</td>
                    <td>
                    @can('isAdmin')
                    <button wire:click="getUser({{ $user->id }})" class="btn btn-sm btn-info text-white mr-1">Edit</button>
                    <button onclick='confirmate()' wire:click="destroy({{ $user->id }}, $conf)"  class="btn btn-sm btn-danger"  >Delete</button>
                    @else
                    <span class="text-danger">Anda tidak dapat melakukan aksi karena bukan admin</span>
                    </td>
                    @endcan
                </tr>
            @endforeach
        </tbody>
    </table>
    <script>
            function confirmate(){

            $conf = confirm('yakin?');


        }
    </script>

    {{ $users->links() }}
</div>
