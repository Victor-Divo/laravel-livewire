<div>
    <form wire:submit.prevent="store">
      	@CSRF
        <input type="hidden" name="" wire:model="userId">
        <div class="form-group">
					<div class="row">
							<div class="col">
								<label for="name">Nama</label>
								<input wire:model="name" 
								type="name" 
								class="form-control @error('name') is-invalid @enderror" 
								id="name" 
								placeholder="Nama anda">
								@error('name')
										<span class="invalid-feedback">
												<strong>{{$message}}</strong>
										</span>
								@enderror
							</div>
							<div class="col">
									<label for="email">Alamat email</label>
									<input wire:model="email" 
									type="email" 
									class="form-control @error('email') is-invalid @enderror"
									id="email" 
									aria-describedby="emailHelp" 
									placeholder="Email anda">
									<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
									@error('email')
											<span class="invalid-feedback">
													<strong>{{$message}}</strong>
											</span>
									@enderror
							</div>
					</div>
				</div>
        <button type="submit" class="btn btn-primary mt-2">Submit</button>
    </form> 
</div>
